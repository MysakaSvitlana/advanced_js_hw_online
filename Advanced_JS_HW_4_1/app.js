//******
// AJAX корисний тим, що можна відправляти запити на сервер без перезавантаження сторінки,
// що значно пришвидшує час і зменьшує використовуваний ресурс при отриманні користувачем інформації чи виконанні подій



//******
// 1. Надіслати AJAX запит на адресу https://ajax.test-danit.com/api/swapi/films та отримати 
//             список усіх фільмів серії Зоряні війни.

// 2.Для кожного фільму отримати з сервера список персонажів, які були показані у цьому фільмі.
//             Список персонажів можна отримати з властивості characters.


// 3.Як тільки з сервера буде отримана інформація про фільми,вивести список їх на екрані.


// 4.Необхідно вказати номер епізоду, назву фільму, а також короткий зміст 
//             -поля episodeId
//             -name 
//             - openingCrawl


// 5.Як тільки з сервера буде отримано інформацію про персонажів будь-якого фільму, 
//             -вивести цю інформацію на екран під назвою фільму.

const urlServ= 'https://ajax.test-danit.com/api/swapi/films '
// 1.
const requestServ = (url) => {
	return fetch(url)
		.then((response) => response.json())
		.catch((error) => console.log(error));
};
//2.
const getCharacters = ((id, urls) => {
	const characterList =  Promise.all(urls.map((elem) => requestServ(elem)))
	characterList.then((data) => {data.forEach(({name}) => {document.getElementById(id).innerHTML +=`<ol>${name}</ol>`})
	})
});
const poster = () => {
	requestServ(urlServ).then((data) => {
		const ul = document.createElement('ul');

		data.forEach(({ id, episodeId, openingCrawl, name, characters }) => {
			const li = document.createElement('li');
			li.innerHTML = `<p>${episodeId}</p> 
			<p style="color: red;">"${name}" :</p>
			<p id="${id}"></p>
			<p>${openingCrawl}</p> `;
			ul.append(li);
			getCharacters(id, characters);
		});
		document.body.append(ul);
	});
};
poster();

