//Поясніть своїми словами, як ви розумієте поняття асинхронності у Javascript
//Лекше пояснити це людині, що до відповіді на це питання думала
//(вчила, вважала), що JS виконує код у файлі рядок за рядком (команда за командою), 
// а виявляється, що це і так,і може здатись, що не так одночасно. Нвсправді ж функції 
//можуть ще переміститись з Call Stack в Web Apis, 
//почекати якусь подію чи результат, потім переміститись в CallBack Queue,
//затриматись там, чекаючи своєї черги, і виконатись в момент, який потрібно для функціоналу.


//****  Написати програму "Я тебе знайду по IP"
// 1.Створити просту HTML-сторінку з кнопкою Знайти по IP.

// 2.Натиснувши кнопку - надіслати AJAX запит за адресою 
//          -- https://api.ipify.org/?format=json, 


// 3.отримати звідти IP адресу клієнта.

// 4.Дізнавшись IP адресу,

// 5.надіслати запит на сервіс 
//          --https://ip-api.com/ 
//          --та отримати інформацію про фізичну адресу.


// 6.Під кнопкою вивести на сторінку інформацію,отриману з останнього запиту 
//          –- континент, 
//          --країна, 
//          --регіон, 
//          --місто, 
//          --район.

// Усі запити на сервер необхідно виконати за допомогою async await.

// const url = "https://api.ipify.org/?format=json";

// const reqInfo = fetch(url)
// .then(d => d.json())
// .then(d => document.querySelector('#ip').innerHTML = d.ip);
// console.log(reqInfo);

const server = 'https://api.ipify.org/?format=json';
const ipAddress = 'http://ip-api.com/';
const btn = document.querySelector('.btn');

const sendRequest = (url) => 
       fetch(url).then(response =>  
       response.json())

btn.addEventListener('click', async () => {
    try{
    const coordinates = await sendRequest (server);
    const location = await sendRequest (`${ipAddress}json/${coordinates.ip}`);
    getLocation(location);  
    }catch(error){
        console.log(error);
    }
   
})
function getLocation({timezone, zip, country, city, regionName}) {
    btn.insertAdjacentHTML(`afterend`,` <div class="container" style="font-size:20px; padding-left:32px">
            <h3>LOCATION</h3>
            <p>TIMI ZONE--: ${timezone}</p>
            <p>Z CODE-------: ${zip}</p>
            <p>COUNTRY---: ${country}</p>
            <p>ORT-----------: ${city}</p>
            <p>REGION-----: ${regionName}</p>
            </div>
    `)
}