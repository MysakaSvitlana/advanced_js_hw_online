class Employee{
    constructor(name, age, salary){
        this.name = name;
        this.age = age;
        this._salary = salary;
    }
}
const employee = new Employee('Lana', 32, 199739);
class Programmer extends Employee{
    constructor(name, age, salary, lang){
        super(name, age, salary);
        this.lang = lang; 
    }
    set lang(valueLang){
        return this._lang = valueLang;
    }
    get lang(){
        return this._lang;
    }
    get salary(){
        return this._salary * 3;
    }
}
const doctor = new Programmer('Ivan', 21, 80000, ['hy', "la"])
console.log(doctor);
const deputy = new Programmer('Egor', 38, 84000, ['bel', 'ua'])


