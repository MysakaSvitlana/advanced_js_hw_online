import React, { useState } from "react";
import PropTypes from "prop-types";
import Card from "../Card/Card";
import "./cardList.scss";

const CardsList = ({
  goods,
  deleteProduct,
  openDeleteModal,
  open,
  deleteFromCart,
  deleteFromSelect,
}) => {
  const [modalOpen, setModalOpen] = useState(false);
  const [choseCard, setChoseCard] = useState(null);
  const [favorite, setFavorite] = useState(false);
  const openModal = (card) => {
    setModalOpen(true);
    setChoseCard(card);
  };
  const closeModal = () => {
    setModalOpen(false);
  };
  const changeFavorite = (card) => {
    if (card) {
      let locFavorite = JSON.parse(localStorage.getItem("favorite"));
      if (locFavorite) {
        const chosenCard = locFavorite.find((obj) => obj.id === card.id);
        const index = locFavorite.indexOf(chosenCard);
        if (index === -1) {
          card.favorite = true;
          setFavorite(!favorite);
          localStorage.setItem(
            "favorite",
            JSON.stringify([...locFavorite, card])
          );
        } else {
          card.favorite = false;
          locFavorite.splice(index, 1);
          setFavorite(!favorite);
          localStorage.setItem("favorite", JSON.stringify(locFavorite));
          if (deleteFromSelect) {
            deleteFromSelect(chosenCard);
          }
        }
        if (locFavorite.length === 0) {
          localStorage.removeItem("favorite");
        }
      } else {
        card.favorite = true;
        setFavorite(!favorite);
        localStorage.setItem("favorite", JSON.stringify([card]));
      }
    }
  };

  const addToCart = () => {
    let cart = JSON.parse(localStorage.getItem("cart"));
    if (cart) {
      const product = cart.find((item) => item.id === choseCard.id);
      const index = cart.indexOf(product);

      if (index === -1) {
        localStorage.setItem("cart", JSON.stringify([...cart, choseCard]));
        closeModal();
      } else if (index !== -1) {
        closeModal();
      }
      if (cart.length === 0) {
        localStorage.removeItem("cart");
      }
    } else {
      localStorage.setItem("cart", JSON.stringify([choseCard]));
    }
    closeModal();
  };

  const goodsList = goods.map((gds) => (
    <Card
      key={gds.id}
      card={gds}
      openModal={openModal}
      closeModal={closeModal}
      modalOpen={modalOpen}
      changeFavorite={() => changeFavorite(gds)}
      addToCart={addToCart}
      deleteProduct={deleteProduct}
      openDeleteModal={openDeleteModal}
      open={open}
      deleteFromCart={deleteFromCart}
    />
  ));
  return (
    <div className="main-wrapper">
      <div className="container">
        <ol className="cards">{goodsList}</ol>
      </div>
    </div>
  );
};

CardsList.propTypes = {
  goods: PropTypes.array.isRequired,
  changeFavorite: PropTypes.func,
  closeModal: PropTypes.func,
  openModal: PropTypes.func,
  modalOpen: PropTypes.bool,
};
CardsList.defaultProps = {
  modalOpen: false,
};
export default CardsList;
