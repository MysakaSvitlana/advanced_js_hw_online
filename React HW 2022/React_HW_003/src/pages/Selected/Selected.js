import React, { useState } from "react";
import CardsList from "../../components/CardsList/CardsList";

function Selected(props) {
  const selectedArr = JSON.parse(localStorage.getItem("favorite"));
  const [locState, setLocState] = useState(false);
  const deleteFromSelect = (card) => {
    if (card) {
      if (selectedArr) {
        const newdArr = selectedArr.filter((el) => el.id !== card.id);
        localStorage.setItem("favorite", JSON.stringify(newdArr));
        console.log(newdArr);
        setLocState(!locState);
        if (newdArr.length === 0) {
          localStorage.removeItem("favorite");
          setLocState(!locState);
        }
      }
    }
  };
  return (
    <>
      {selectedArr ? (
        <CardsList
          goods={selectedArr}
          deleteProduct={false}
          deleteFromSelect={deleteFromSelect}
        />
      ) : (
        <h2 className="empty-cart">You have't selected product </h2>
      )}
    </>
  );
}

export default Selected;
