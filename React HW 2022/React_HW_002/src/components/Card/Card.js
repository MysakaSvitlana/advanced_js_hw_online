import React, { Component } from "react";
import PropTypes from "prop-types";
import "./card.scss";
import Button from "../Button/Button";
import Modal from "../Modal/Modal";
import Icon from "../Icon/Icon";
class Card extends Component {
  render() {
    const {
      card,
      openModal,
      modalOpen,
      closeModal,
      changeFavorite,
      addToCart,
    } = this.props;

    return (
      <li className="card">
        <div className="card__img-wrap">
          <img
            src={card.url}
            alt="GoodsPicture"
            className="card__img"
            height="250"
          />
        </div>

        <div className="card__wrap">
          <h5 className="card__title">{card.title}</h5>
          <Button openModal={openModal} card={card} />
          {modalOpen && <Modal closeModal={closeModal} addToCart={addToCart} />}
          <Icon
            type="star"
            filled={card.favorite}
            changeFavorite={changeFavorite}
          />
          <div className="card__info-wrap">
            <p className="card__info">Цена:</p>
            <span className="card__info-price">{card.price}</span>
          </div>
          <div className="card__info-wrap">
            <p className="card__info-arcicle">артикyл:</p>
            <span className="card__info-arcicle">{card.article}</span>
          </div>
        </div>
      </li>
    );
  }
}

Card.propTypes = {
  closeModal: PropTypes.func.isRequired,
  openModal: PropTypes.func.isRequired,
  modalOpen: PropTypes.bool,
  card: PropTypes.shape({
    id: PropTypes.number.isRequired,
    title: PropTypes.string.isRequired,
    article: PropTypes.string.isRequired,
    url: PropTypes.string.isRequired,
    price: PropTypes.string.isRequired,
    color: PropTypes.string,
    favorite: PropTypes.bool.isRequired,
  }).isRequired,
};
Card.defaultProps = {
  modalOpen: false,
  color: "white",
};
export default Card;
