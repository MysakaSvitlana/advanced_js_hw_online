import React from "react";
import { connect } from "react-redux";
import CardsList from "../../components/CardsList/CardsList";
import "./cart.scss";
import actions from "../../store/actions";

function Cart({ isOpen, cardToDelete, dispatch }) {
  const cartArr = JSON.parse(localStorage.getItem("cart"));

  const openDeleteModal = (card) => {
    dispatch(actions.cartIsOpen(!isOpen));
    dispatch(actions.setCardToDelete(card));
  };
  const deleteFromCart = () => {
    if (cardToDelete) {
      if (cartArr) {
        const newCartArr = cartArr.filter((el) => el.id !== cardToDelete.id);
        localStorage.setItem("cart", JSON.stringify(newCartArr));
        console.log(newCartArr);
        openDeleteModal();
        if (newCartArr.length === 0) {
          localStorage.removeItem("cart");
        }
      }
    }
  };
  return (
    <>
      {cartArr ? (
        <div>
          <div className="cart-container">
            <div className="cart-img"></div>
            <span className="cart-count">{cartArr.length}</span>
          </div>
          <CardsList
            goods={cartArr}
            deleteProduct={true}
            openDeleteModal={openDeleteModal}
            deleteFromCart={deleteFromCart}
          />
        </div>
      ) : (
        <h2 className="empty-cart">Your cart is empty</h2>
      )}
    </>
  );
}

const mapStateToProps = (state) => {
  return {
    isOpen: state.cart.isOpen,
    cardToDelete: state.cart.cardToDelete,
  };
};
export default connect(mapStateToProps)(Cart);
