import types from "./types";
const changeSelectedToremove = (payload) => ({
  type: types.CHANGE_SELECTED_TO_REMOVE,
  payload,
});
const cartIsOpen = (payload) => ({
  type: types.CART_IS_OPEN,
  payload,
});
const setCardToDelete = (payload) => ({
  type: types.SET_CARD_TO_DELETE,
  payload,
});
const setGoodsData = (payload) => ({
  type: types.SET_GOODS_DATA,
  payload,
});
const setGoodsLoading = (payload) => ({
  type: types.SET_GOODS_LOADING,
  payload,
});
const setModalIsOpen = (payload) => ({
  type: types.SET_MODAL_IS_OPEN,
  payload,
});
const setChosenCard = (payload) => ({
  type: types.SET_CHOSEN_CARD,
  payload,
});
const changeFavoriteInCard = (payload) => ({
  type: types.CHANGE_FAVORITE_IN_CARD,
  payload,
});
export default {
  changeSelectedToremove,
  cartIsOpen,
  setCardToDelete,
  setGoodsData,
  setGoodsLoading,
  setModalIsOpen,
  setChosenCard,
  changeFavoriteInCard,
};
