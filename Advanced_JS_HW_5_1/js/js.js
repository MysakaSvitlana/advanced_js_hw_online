// 1.При відкритті сторінки необхідно отримати з сервера список всіх користувачів та загальний список публікацій. 
//                 Для цього потрібно надіслати GET запит на наступні дві адреси:
//                             -- https://ajax.test-danit.com/api/json/users
//                             -- https://ajax.test-danit.com/api/json/posts

// 2.Після завантаження всіх користувачів та їх публікацій необхідно відобразити всі публікації на сторінці.

// 3.Кожна публікація має бути відображена у вигляді картки (приклад: https://prnt.sc/q2em0x), та включати :
//                        -- заголовок,
 //                       -- текст,
 //                       -- ім'я, 
 //                       -- прізвище та 
 //                       -- імейл користувача, який її розмістив.


// 4.На кожній картці повинна бути іконка або кнопка, яка дозволить видалити цю картку зі сторінки. 
//            --При натисканні на неї необхідно надіслати DELETE запит на адресу 
//                       -- https://ajax.test-danit.com/api/json/posts/${postId}. 

//5.Після отримання підтвердження із сервера (запит пройшов успішно), картку можна видалити,використовуючи JS

const container = document.querySelector('#container');
class ResponseApi {
	constructor() {this._baseUrl = 'https://ajax.test-danit.com/api/json/';}
	async request(method, url) {
		const response = await fetch(this._baseUrl + url, {method, });
		return response;
	}
	get(url) {
		return this.request('GET', url);
	}
	delete(url) {
		return this.request('DELETE', url);}}
class Article extends ResponseApi {
	constructor(container) {
		super();
	this.container = container;}
	async getInfo() {
		try {
			return await Promise.all([super.get('users'), super.get('posts')]);
		} catch (e) {
			console.log(e);}}
	async deletePost(postId, e) {
		try {
			const response = await super.delete(`posts/${postId}`);
			console.log(postId, response.status);
			if(response.ok) {
				e.target.closest('li').remove();
			} else {
				throw response.status;
			}
 		} catch(e) {
			console.log(e);
		}
	}
	async visualisation() {
		const ul = document.createElement('ul');
		const [users1, posts1] = await this.getInfo();
		const users = await users1.json();
		const posts = await posts1.json();
		
		users.forEach(({ id, name, email }) => {
			const filteredPosts = posts.filter(({ userId }) => userId == id);
			filteredPosts.forEach(({ id, title, body }) => {
				const li = document.createElement('li');
				li.className = 'item'
				const buttonDelete = document.createElement('button');
				buttonDelete.className = 'button-delete'
				buttonDelete.innerHTML = 'X';
				buttonDelete.addEventListener('click', (e) => {
					const postId = e.target.closest('li').id;
					this.deletePost(postId, e);});
				li.setAttribute('id', id);
				li.innerHTML = `
					<span class='item__name'>${name}</span> <span class='item__email'>${email}</span>
					<h2 class='item__title'>${title}</h2> <p class='item__body'>${body}</p>`;
				li.append(buttonDelete);
				ul.append(li);
			});
		});
		this.container.append(ul);}}

const post = new Article (container);
post.visualisation();

